<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use UserClient;

class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var  UserClient $userClient */
    protected $userClient;

    /** @var array data */
    protected $data;

    protected $singleUser;

    public function startup()
    {
        parent::startup();
        $this->userClient = new UserClient('http://localhost:8000');
    }

    public function actionDefault()
    {
        $result = $this->userClient->getAllUsers();
        $this->data = $result->content;
    }

    public function actionEdit($id)
    {
        $result = $this->userClient->getUserById($id);
        $this->singleUser = $result['content'];
    }

    public function renderEdit()
    {
        /** @var Form $form */
        $form = $this['updateForm'];
        $form->setDefaults($this->singleUser);
    }

    public function renderDefault()
    {
        $this->template->data = $this->data;
    }

    /**
     * Form for specification of a new user
     * @param $name
     */
    public function createComponentCreateForm($name)
    {
        $form = new Form($this, $name);
        $form->addText('name', 'Name');
        $form->addPassword('password', 'Password');
        $form->addEmail('email', 'Email');
        $form->addSelect('role', 'Role', ['user' => 'User', 'admin' => 'Admin']);
        $form->addSubmit('submit');

        $form->onSuccess[] = [$this, 'createFormSubmit'];
    }

    /**
     * New user submit form
     * @param Form $form
     */
    public function createFormSubmit(Form $form)
    {
        $values = $form->getValues();

        $response = $this->userClient->createNewUser($values->name, $values->password, $values->email, $values->role);

        $this->flashMessage($response['message'], 'primary');

        $this->redirect('default');
    }

    /**
     * Form for edit existing user
     * @param $name
     */
    public function createComponentUpdateForm($name)
    {
        $form = new Form($this, $name);
        $form->addText('name', 'Name');
        $form->addPassword('password', 'Password');
        $form->addEmail('email', 'Email');
        $form->addSelect('role', 'Role', ['user' => 'User', 'admin' => 'Admin']);
        $form->addHidden('id');
        $form->addSubmit('submit');

        $form->onSuccess[] = [$this, 'updateFormSubmit'];
    }

    /**
     * Edit user form submit
     * @param Form $form
     */
    public function updateFormSubmit(Form $form)
    {
        $values = $form->getValues(TRUE);

        $response = $this->userClient->updateUserById($values['id'], $values);

        $this->flashMessage($response['message'], 'primary');

        $this->redirect('default');
    }

    /**
     * Delete user by ID
     *
     * @param $id
     */
    public function actionDelete($id)
    {
        $this->userClient->deleteUserById($id);

        $this->flashMessage('User deleted.', 'danger');

        $this->redirect('default');
    }

}
