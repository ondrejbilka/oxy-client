<?php
use GuzzleHttp\Client;

class UserClient extends \Nette\Object
{
    /** @var Client $client */
    protected $client;

    /**
     * UserClient constructor.
     *
     * @param $baseUri
     */
    public function __construct($baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri, 'http_errors'=>false]);
    }

    /**
     * Obtain all users from api
     *
     * @return int|mixed
     */
    public function getAllUsers()
    {
        $response = $this->client->get('/user');

        $status = $response->getStatusCode();

        if(in_array($status, [200, 204]))
        {
            return json_decode($response->getBody()->getContents());
        } else{
            return $status;
        }
    }

    /**
     * Obtain user details by id
     *
     * @param $id
     * @return int|mixed
     */
    public function getUserById($id)
    {
        $response = $this->client->get('/user/'. $id);

        $status = $response->getStatusCode();

        if(in_array($status, [200, 204]))
        {
            return json_decode($response->getBody()->getContents(), TRUE);
        } else{
            return $status;
        }
    }

    /**
     * Create new user
     *
     * @param $name
     * @param $password
     * @param $email
     * @param $role
     * @return mixed
     */
    public function createNewUser($name, $password, $email, $role)
    {
        $response = $this->client->request('POST', '/user', [
            'json' => ['name' => $name, 'password' => $password, 'email' => $email, 'role' => $role]
        ]);

        return json_decode($response->getBody()->getContents(), TRUE);

    }

    /**
     * Update existing user by ID
     *
     * @param array $values
     * @return int
     */
    public function updateUserById($id, array $values)
    {
        $id = $values['id'];
        unset($values['id']);
        foreach($values as $k => $v){
            if($v === ''){
                unset($values[$k]);
            }
        }

        $response = $this->client->request('PUT', '/user/' . $id, [
            'json' => $values
        ]);

        return $response->getStatusCode();
    }

    public function deleteUserById($id)
    {
        $response = $this->client->delete('/user/'. $id);
        return $response->getStatusCode();
    }

}